package main

import (
	"encoding/base64"
	"log"
	"net/http"
	"os"

	"brankas-test/config"
	"brankas-test/controller"
	"brankas-test/data"
	"brankas-test/domain/usecase"
)

var (
	port = ":8081"
)

func main() {

	// Load auth key
	auth := os.Getenv("AUTH")
	if auth == "" {
		log.Fatalf("AUTH config is required")
	}

	// Load config file using viper
	configPath := config.GetConfigPath(os.Getenv("config"))
	cfg, err := config.GetConfig(configPath)
	if err != nil {
		log.Fatalf("Loading config: %v", err)
	}

	// Initialize postgres
	db, err := data.NewFileDB(cfg)
	if err != nil {
		log.Fatalf("Initialize Postgres database: %v", err)
	}

	// Initialize database table
	db.AutoMigrate(data.File{})

	encodedAuth := base64.StdEncoding.EncodeToString([]byte(auth))
	u := usecase.NewUsecase(encodedAuth, db)
	c := controller.NewController(encodedAuth,u)

	// Initialize http handler function
	http.HandleFunc("/", c.Index)
	http.HandleFunc("/upload", c.Upload)

	log.Println("Http server is listening on port", port)

	// Run http server
	log.Fatalf("%v",http.ListenAndServe(port, nil))
}

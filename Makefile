install:
	go mod tidy
	docker-compose up -d

run:
	AUTH=secret go run main.go
# Brankas Test
Brank.as golang skill code test

## Contents
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Run](#run)
  * [Change Secret Key](#change-secret-key)

## Getting Started
### Prerequisites
1.  Please make sure you have [Go](https://golang.org/doc/install) installed on your system.
2.  Please make sure you have [Docker](https://docs.docker.com/engine/install/) installed on your system.
3.  Please make sure you have [Docker Compose](https://docs.docker.com/compose/install/) installed on your system.

### Installation
1.  Open your terminal and navigate to this project folder.
```bash
$ cd brankas-test
```
2.  Execute the following command to setup the installation and required environment.
```bash
$ make install
go mod tidy
docker-compose up -d
Creating network "brankas-test_default" with the default driver
Creating brankas-db ... done 
```
3.  Run the following command to verify docker container is up and running.
```bash
$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED         STATUS        PORTS                                       NAMES
a7545dd1b69e   postgres:alpine   "docker-entrypoint.s…"   2 seconds ago   Up 1 second   0.0.0.0:5442->5432/tcp, :::5442->5432/tcp   brankas-db
```
## Run
1.  Execute the following command on terminal to run the application.
```bash
$ make run
AUTH=secret go run main.go                  

(/home/mushoffa/go/pkg/mod/github.com/mushoffa/go-library@v0.0.0-20211116231353-558a8237606e/database/postgres.go:94) 
[2021-11-24 00:22:47]  [137.91ms]  CREATE TABLE "files" ("id" serial,"created" timestamp with time zone,"name" text,"type" text,"size" bigint , PRIMARY KEY ("id"))  
[0 rows affected or returned ]        
2021/11/24 00:22:47 Http server is listening on port :8081 
```

2.  Open your browser and navigate to [localhost:8081](http://localhost:8081)
![](assets/index_html.png)

3.  Fill the form and press 'Upload' button, if the process success the terminal will display database insertion logs.
```bash
[2021-11-24 00:14:18]  [3.11ms]  INSERT INTO "files" ("created","name","type","size") VALUES ('2021-11-24 00:14:18','D4YDF.png','image/png',106037) RETURNING "files"."id"  
[1 rows affected or returned ]
```
### Change Secret Key
1. To change secret key, open the [Makefile](Makefile).
2. Change the `AUTH` key to your desired value.
```bash
install:
	go mod tidy
	docker-compose up -d

run:
	AUTH=secret go run main.go
```

3. Re-run the program to test your new secret key.

package controller

import (
	"log"
	"net/http"
	"strings"
	"text/template"
	"time"

	"brankas-test/data"
	"brankas-test/domain/usecase"
)

type Controller interface {
	Index(http.ResponseWriter, *http.Request)
	Upload(http.ResponseWriter, *http.Request)
}

type controller struct {
	auth string
	u usecase.Usecase
}

func NewController(auth string, u usecase.Usecase) Controller {
	return &controller{auth, u}
}

func (c *controller) Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	// Serve static page from index.html file
	var page = template.Must(template.ParseFiles("index.html"))
	// page, _ := template.ParseFiles("index.html")
	var err = page.Execute(w, c.auth)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (c *controller) Upload(w http.ResponseWriter, r *http.Request) {
	// Ensure only POST method is allowed to access this endpoint
	if r.Method != "POST" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	// Get auth value from form input field
	auth := r.FormValue("auth")

	// Trim the trailing character
	auth = strings.Trim(auth, "/")

	// Validate whether auth from input field matches from env code
	if err := c.u.ValidateAuthorization(auth); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	// Get uploaded file and its header
	file, multipartFileHeader, err := r.FormFile("data")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	// Create a buffer to store the header of the file in
	fileHeader := make([]byte, 512)

	// Copy the headers into the FileHeader buffer
	if _, err := file.Read(fileHeader); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// set position back to start.
	if _, err := file.Seek(0, 0); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := data.File{
		Created: time.Now(),
		Name: multipartFileHeader.Filename,
		Type: http.DetectContentType(fileHeader),
		Size: multipartFileHeader.Size,
	}

	if err := c.u.Validate(&data); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	log.Printf("Name: %#v\n", multipartFileHeader.Filename)
	log.Printf("Size: %#v\n", multipartFileHeader.Size)
	log.Printf("MIME: %#v\n", http.DetectContentType(fileHeader))

	c.u.StoreFileData(&data)

	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("Success"))
}
package data

import "time"

type File struct {
	ID 			int32 		`gorm:"id"`
	Created 	time.Time 	`gorm:"created"`
	Name 		string 		`gorm:"file_name"`
	Type 		string 		`gorm:"file_type"`
	Size 		int64 		`gorm:"file_size"`
}

func (db *File) TableName() string {
	return "files"
}
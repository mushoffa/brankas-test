package data

import (
	"brankas-test/config"

	"github.com/mushoffa/go-library/database"
)

type FileDB struct {
	database.Database
}

func NewFileDB(cfg *config.Config) (database.Database, error) {
	db, err := database.NewPostgres(
		cfg.Postgres.PostgresHost,
		cfg.Postgres.PostgresPort,
		cfg.Postgres.PostgresDbName,
		cfg.Postgres.PostgresUser,
		cfg.Postgres.PostgresPassword,
		false,
	)

	if err != nil {
		return nil, err
	}

	return &FileDB{db}, nil
}
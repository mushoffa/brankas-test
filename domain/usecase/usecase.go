package usecase

import (
	"errors"
	"strings"
	"log"

	// This dependency graph is anti-pattern of clean architecture
	// Implemented on this project for simplicity
	"brankas-test/data"

	"github.com/mushoffa/go-library/database"
)

const (
	MAX_FILE_SIZE = 8 * 1024 * 1024 // 8 MB
)

type Usecase interface {
	StoreFileData(*data.File)
	ValidateAuthorization(string) error
	ValidateFileType(string) error
	ValidateFileSize(int64) error
	Validate(*data.File) error
}

type usecase struct {
	auth string
	db database.Database
}

func NewUsecase(auth string, db database.Database) Usecase {
	return &usecase{auth, db}
}

func (u *usecase) StoreFileData(data *data.File) {
	if err := u.db.Create(data); err != nil {
		log.Println("Error insert to database: ", err)
	}	
}

func (u *usecase) ValidateAuthorization(auth string) error{
	if u.auth != auth {
		return errors.New("Invalid credentials")
	}

	return nil
}

func (u *usecase) ValidateFileType(fileType string) error {
	if strings.Contains(fileType, "image") {
		return nil
	}

	return errors.New("Invalid file type")
}

func (u *usecase) ValidateFileSize(fileSize int64) error {
	if fileSize <= MAX_FILE_SIZE {
		return nil
	}

	return errors.New("File size exceeds maximum limit")
}

func (u *usecase) Validate(file *data.File) error {
	if err := u.ValidateFileType(file.Type); err != nil {
		return err
	}

	if err := u.ValidateFileSize(file.Size); err != nil {
		return err
	}

	return nil
}